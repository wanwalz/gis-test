import sys
import geojson

def compute_bounding_box(coords):
  """Compute the bounding box of a list of coordinates"""
  if len(coords) == 0: return []
  DIM = len(coords[0])
  mins = []
  maxs = []
  for i in range(DIM):
    v = list(map(lambda x: x[i], coords))
    mins.append(min(v))
    maxs.append(max(v))

  return mins + maxs

def bounding_box(geometry):
  """Convert a geometry to a feature with bounding box"""
  coords = list(geojson.utils.coords(geometry))
  bbox = compute_bounding_box(coords)
  return geojson.Feature(geometry=geometry, bbox=bbox)

if __name__ == '__main__':
  if len(sys.argv) < 2 or len(sys.argv) > 3:
    sys.exit('Usage: %s input.geo.json [output.geo.json]' % sys.argv[0])

  input_geo = None
  with open(sys.argv[1], 'r') as input_file:
    input_geo = geojson.loads(input_file.read())

  if input_geo is None: sys.exit('Could not read %s' % sys.argv[1])

  output_geo = bounding_box(input_geo)

  if len(sys.argv) == 3:
    with open(sys.argv[2], 'w') as output_file:
      output_file.write(geojson.dumps(output_geo))
  else:
    print(geojson.dumps(output_geo, indent=2))