import sys
import os
import re
import geojson

from bounding_box import bounding_box

def translate(points, x, y):
  """Translate geo to pixel"""

  # Two linear translations for x and y
  # we're looking for two points in the x and y translations
  # to compute to coefs of the line
  x1,y1,a1,b1 = points[0]
  x2,y2,a2,b2 = points[0]
  for point in points:
    xt,yt,at,bt = point
    if xt != x1:
      x2 = xt
      a2 = at

    if yt != y1:
      y2 = yt
      b2 = bt
    if (((x2 - x1) != 0) and ((y2 - y1) != 0)):
      break

  xa = (a2 - a1)/(x2 - x1)
  xb = a1 - xa*x1
  ya = (b2 - b1)/(y2 - y1)
  yb = b1 - ya*y1

  return (x*xa + xb, y*ya + yb)

if __name__ == '__main__':
  if len(sys.argv) < 4 or len(sys.argv) > 4:
    sys.exit('Usage: %s file.tab geo.json crop.jpg' % sys.argv[0])

  tab = None
  with open(sys.argv[1], 'r') as tab_file:
    tab = tab_file.read()

  if tab is None: sys.exit('Could not read %s' % sys.argv[1])

  geometry = None
  with open(sys.argv[2], 'r') as geometry_file:
    geometry = geojson.loads(geometry_file.read())

  if geometry is None: sys.exit('Could not read %s' % sys.argv[2])
  feature = bounding_box(geometry)
  bbox = feature.bbox

  output = sys.argv[3]

  image_file = 0
  points = []
  for line in tab.split('\n'):
    file_re = re.match(r'File\s+"(.*)"', line)
    if file_re: image_file = file_re.group(1)
    points_re = re.match(r'\(([\d\.]+),([\d\.]+)\)\s+\(([\d\.]+),([\d\.]+)\)\s+.+', line)
    if points_re:
      p = tuple([float(points_re.group(i+1)) for i in range(4)])
      points.append(p)

  pixel_box = [0,0,0,0]
  pixel_box[0], pixel_box[1] = translate(points, bbox[0], bbox[1])
  pixel_box[2], pixel_box[3] = translate(points, bbox[2], bbox[3])
  x = pixel_box[0]
  y = pixel_box[0]
  w = pixel_box[2] - pixel_box[0]
  h = pixel_box[3] - pixel_box[1]

  image_file = '/'.join(sys.argv[1].split('/')[:-1]) + '/' + image_file
  os.system('convert %s -crop %ix%i+%i+%i %s' % (image_file, w,h,x,y, sys.argv[3]))