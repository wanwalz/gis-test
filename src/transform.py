import sys
import geojson
import pyproj

def transform_coord(source, dest, x, y):
  """Transfrom coordinates from epsg source to dest"""
  inProj = pyproj.Proj(init='epsg:%s' % source)
  outProj = pyproj.Proj(init='epsg:%s' % dest)
  return pyproj.transform(inProj,outProj,x,y)

def transform_geometry(source, dest, geometry):
  """Transfrom Geojson geometry from epsg source to dest"""
  return geojson.utils.map_tuples(lambda c:
    transform_coord(source, dest, c[0], c[1]),
    geometry
  )

if __name__ == '__main__':
  if len(sys.argv) < 4 or len(sys.argv) > 5:
    sys.exit('Usage: %s source dest input.geo.json [output.geo.json]' % sys.argv[0])

  source_epsg = sys.argv[1]
  dest_epsg = sys.argv[2]

  input_geo = None
  with open(sys.argv[3], 'r') as input_file:
    input_geo = geojson.loads(input_file.read())

  if input_geo is None: sys.exit('Could not read %s' % sys.argv[1])

  output_geo = transform_geometry(source_epsg, dest_epsg, input_geo)

  if len(sys.argv) == 5:
    with open(sys.argv[4], 'w') as output_file:
      output_file.write(geojson.dumps(output_geo))
  else:
    print(geojson.dumps(output_geo, indent=2))