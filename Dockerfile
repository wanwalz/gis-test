FROM python:3.7

RUN apt-get update -yy && apt-get upgrade -yy
RUN apt-get install -yy build-essential
RUN apt-get remove -yy imagemagick

WORKDIR /opt
RUN curl -sSL https://github.com/ImageMagick/ImageMagick/archive/7.0.8-59.tar.gz | tar xz
RUN cd ImageMagick-7.0.8-59 && ./configure && make && make install && ldconfig /usr/local/lib
RUN cd ImageMagick-7.0.8-59 && convert logo: logo.gif

RUN pip install pipenv

WORKDIR /usr/src/app

COPY Pipfile Pipfile.lock ./
RUN pipenv install

COPY . .
ENTRYPOINT ["./run.sh"]