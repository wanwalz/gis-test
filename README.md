# Docker
```bash
$ docker build -t gis/test .
$ docker run --rm -it -v $PWD/out:/usr/src/app/out gis/test
```

# Setup
```bash
$ pipenv install
```

# Geometry processing

## 1. Projection

### a.

```bash
$ pipenv run python src/transform.py 4326 2154 data/namr.EPSG4326.json
```

### b.

```bash
$ pipenv run python src/transform.py 2154 4326 data/collegevinci.mpoly.ESPG2154.json
```

## 2. Bounding box

```bash
$ pipenv run python src/bounding_box.py data/collegevinci.poly.ESPG2154.json
```

or

```bash
$ pipenv run python src/transform.py 2154 4326 data/collegevinci.poly.ESPG2154.json out/collegevinci.poly.ESPG4326.json
$ pipenv run python src/bounding_box.py out/collegevinci.poly.ESPG4326.json
```

## 3. Image processing
```bash
$ pipenv run python src/transform.py 4326 2154 data/sanciprianu.EPSG4326.json out/sanciprianu.EPSG2154.json
$ pipenv run python src/crop.py image/2A-2016-1225-6085-LA93-0M20-E080.tab out/sanciprianu.EPSG2154.json out/sanciprianu.jpg
```