#!/bin/bash

echo 1.a
pipenv run python src/transform.py 4326 2154 data/namr.EPSG4326.json out/namr.EPSG2154.json

echo 1.b
pipenv run python src/transform.py 2154 4326 data/collegevinci.mpoly.ESPG2154.json out/collegevinci.mpoly.ESPG4326.json

echo 2
pipenv run python src/transform.py 2154 4326 data/collegevinci.poly.ESPG2154.json out/collegevinci.poly.ESPG4326.json
pipenv run python src/bounding_box.py out/collegevinci.poly.ESPG4326.json out/collegevinci.feature.ESPG4326.json

echo 3
pipenv run python src/transform.py 4326 2154 data/sanciprianu.EPSG4326.json out/sanciprianu.EPSG2154.json
pipenv run python src/crop.py image/2A-2016-1225-6085-LA93-0M20-E080.tab out/sanciprianu.EPSG2154.json out/sanciprianu.jpg
